defmodule Lynx.Accounts do
  @moduledoc """
  The Accounts context.
  """
  import Plug.Conn
  import Ecto.Query, warn: false
  alias Lynx.Repo

  alias Lynx.Accounts.{User, Guardian}

  ####### Dealing with CRUD Functions #######
  def list_users do
    Repo.all(User)
  end

  def get_user!(id), do: Repo.get!(User, id)

  def get_user(email) do
    query = Ecto.Query.from(u in User, where: u.email == ^email)
    Repo.one(query)
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  ####### Dealing with authentication #######
  def get_current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end

  def authenticate_user(email, given_password) do
    query = Ecto.Query.from(u in User, where: u.email == ^email)

    Repo.one(query)
    |> check_password(given_password)
  end

  ## Use auth0 callback to create a user ##
  #   def find_or_add(%{name: email, id: _auth0_id, avatar: _avatar} = attrs) do
  #     case get_user(email) do
  #       %User{email: email} -> IO.puts("***~~~ User exists in DB ~~~***")
  #       _ -> User.changeset(%User{email: email, display_name: email}, attrs) |> Repo.insert()
  #     end
  #   end

  def login(conn, user) do
    conn
    |> Guardian.Plug.sign_in(user)
    # |> assign(:current_user, user)
    |> assign(:user_signed_in?, true)
    |> put_session(:user_id, user.id)
    # |> put_session(:current_user, user)
    |> configure_session(renew: true)
    |> put_user_token(user)
  end

  def logout(conn) do
    conn
    |> configure_session(drop: true)
    |> Guardian.Plug.sign_out()
  end

  def load_current_user(conn, _) do
    conn
    |> assign(:current_user, Guardian.Plug.current_resource(conn))
    |> put_user_token(Guardian.Plug.current_resource(conn))
  end

  ####### Private functions #######

  defp check_password(nil, _), do: {:error, "Incorrect email or password"}

  defp check_password(user, given_password) do
    case Argon2.verify_pass(given_password, user.password_hash) do
      true -> {:ok, user}
      false -> {:error, "Incorrect email or password"}
    end
  end

  # defp put_user_token(conn, user) do
  #   token = Phoenix.Token.sign(conn, "user socket", user.id)

  #   conn
  #   |> assign(:user_token, token)
  # end

  defp put_user_token(conn, user) do
    if user != nil do
      token = Phoenix.Token.sign(conn, "user socket", user.id)
      assign(conn, :user_token, token)
    else
      conn
    end
  end
end
