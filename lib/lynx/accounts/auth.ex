defmodule Lynx.Accounts.Auth do
  @moduledoc """
  Boundry for User Authentication
  """
  alias Lynx.Accounts.{Guardian}
  import Ecto.{Query, Changeset}, warn: false
  #   import Plug.Conn

  #   def login(params, repo) do
  #     user = repo.get_by(User, email: String.downcase(params["email"]))

  #     case authenticate(user, params["password"]) do
  #       true -> {:ok, user}
  #       _ -> :error
  #     end
  #   end

  #   defp authenticate(user, password) do
  #     case user do
  #       nil -> false
  #       _ -> Encryption.validate_password(password, user.password_hash)
  #     end
  #   end

  ## Helper functions for view
  def current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end

  def logged_in?(conn), do: !!current_user(conn)
end
