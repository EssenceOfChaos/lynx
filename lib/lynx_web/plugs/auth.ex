defmodule Lynx.Plug.Authentication do
  @moduledoc false
  use Guardian.Plug.Pipeline,
    otp_app: :lynx,
    error_handler: Lynx.GuardianErrorHandler,
    module: Lynx.Accounts.Guardian

  plug Guardian.Plug.VerifySession
  plug Guardian.Plug.LoadResource, allow_blank: true
end
