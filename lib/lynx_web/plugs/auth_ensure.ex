defmodule Lynx.Plug.EnsureAuthentication do
  @moduledoc false
  use Guardian.Plug.Pipeline,
    otp_app: :lynx,
    module: Lynx.Accounts.Guardian

  plug(Guardian.Plug.VerifySession, claims: %{"typ" => "access"})
  plug(Guardian.Plug.EnsureAuthenticated)
  plug(Guardian.Plug.LoadResource)
end
