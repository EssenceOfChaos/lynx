defmodule LynxWeb.PageController do
  use LynxWeb, :controller
  alias Lynx.Accounts

  def index(conn, _params) do
    user = Accounts.get_current_user(conn)

    conn
    |> IO.inspect()
    |> render("index.html", user: user)
  end
end
