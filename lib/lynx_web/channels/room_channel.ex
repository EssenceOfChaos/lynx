defmodule LynxWeb.RoomChannel do
  @moduledoc """
  The Room Channel for group chat
  """
  use LynxWeb, :channel
  alias LynxWeb.Presence

  def join("room:lobby", payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)

      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (room:lobby).
  def handle_in("shout", payload, socket) do
    broadcast(socket, "shout", payload)
    {:noreply, socket}
  end

  def handle_in("new_msg", %{"body" => body}, socket) do
    broadcast!(socket, "new_msg", %{
      body: body,
      user: socket.assigns.current_user.display_name
    })

    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    push(socket, "presence_state", Presence.list(socket))
    IO.inspect(socket.assigns.current_user)

    {:ok, _} =
      Presence.track(socket, socket.assigns.current_user.id, %{
        name: socket.assigns.current_user.display_name,
        online_at: inspect(System.system_time(:second))
      })

    {:noreply, socket}
  end

  #   intercept ["user_joined"]

  #   def handle_out("user_joined", msg, socket) do
  #     if Accounts.ignoring_user?(socket.assigns[:user], msg.user_id) do
  #       {:noreply, socket}
  #     else
  #       push(socket, "user_joined", msg)
  #       {:noreply, socket}
  #     end
  #   end

  ## Handle new_bid which comes from the socket.js file from channel.on("new_bid")

  #   def handle_in("new_id", params, socket) do
  #     broadcast!(socket, "new_bid", params)
  #     {:noreply, socket}
  #   end

  ### Private ###
  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
