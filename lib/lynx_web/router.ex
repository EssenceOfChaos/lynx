defmodule LynxWeb.Router do
  @moduledoc """
  Handles application routing between controllers and views
  """
  use LynxWeb, :router
  alias Lynx.Plug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Plug.Authentication
    # plug :put_user_token
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug Plug.EnsureAuthentication
  end

  ## Not currently using Auth0 because return value
  ## %{id: id, email: email, name: name} can't be validated by ecto

  #   scope "/auth", LynxWeb do
  #     pipe_through :browser

  #     get "/:provider", SessionController, :request
  #     get "/:provider/callback", SessionController, :callback
  #     post "/:provider/callback", SessionController, :callback
  #   end

  scope "/", LynxWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources("/users", UserController, only: [:new, :create, :show, :index])
    resources("/session", SessionController, only: [:create, :new, :delete])
  end

  scope "/", LynxWeb do
    pipe_through [:browser, :authenticated]
    resources("/users", UserController, only: [:delete, :edit, :update])
    resources("/session", SessionController, only: [:delete])
  end

  # Other scopes may use custom stacks.
  # scope "/api", LynxWeb do
  #   pipe_through :api
  # end

  ### Private functions ###
  #   defp put_user_token(conn, _) do
  #     if current_user = conn.assigns[:current_user] do
  #       token = Phoenix.Token.sign(conn, "user socket", current_user.id)
  #       assign(conn, :user_token, token)
  #     else
  #       conn
  #     end
  #   end
end
