defmodule LynxWeb.PageView do
  use LynxWeb, :view

  def gravatar(email) do
    hash =
      email
      |> String.trim()
      |> String.downcase()
      |> :erlang.md5()
      |> Base.encode16(case: :lower)

    img = "https://www.gravatar.com/avatar/#{hash}?s=150&d=identicon"
    img_tag(img)
  end

  def get_time() do
    {:ok, datetime} = DateTime.now("Etc/UTC")
    datetime
  end
end
