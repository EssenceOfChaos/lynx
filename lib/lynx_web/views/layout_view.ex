defmodule LynxWeb.LayoutView do
  use LynxWeb, :view

  def title() do
    "Lynx"
  end
end
