import { Socket, Presence } from 'phoenix';

document.addEventListener('DOMContentLoaded', event => {
    // let socket = new Socket('/socket', { params: { token: window.userToken } });
    let token = document
        .querySelector('meta[name=channel_token]')
        .getAttribute('content');
    // let token = $('meta[name=channel_token]').attr('content');
    let socket = new Socket('/socket', {
        params: {
            token: token
        },
        logger: (kind, msg, data) => {
            console.log(`${kind}: ${msg}`, data);
        }
    });
    // When you connect, you'll often need to authenticate the client.
    // For example, imagine you have an authentication plug, `MyAuth`,
    // which authenticates the session and assigns a `:current_user`.
    // If the current user exists you can assign the user's token in
    // the connection for use in the layout.
    //
    // In your "lib/web/router.ex":
    //
    //     pipeline :browser do
    //       ...
    //       plug MyAuth
    //       plug :put_user_token
    //     end
    //
    //     defp put_user_token(conn, _) do
    //       if current_user = conn.assigns[:current_user] do
    //         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
    //         assign(conn, :user_token, token)
    //       else
    //         conn
    //       end
    //     end
    //
    // Now you need to pass this token to JavaScript. You can do so
    // inside a script tag in "lib/web/templates/layout/app.html.eex":
    //
    //     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
    //
    // You will need to verify the user token in the "connect/3" function
    // in "lib/web/channels/user_socket.ex":
    //
    //     def connect(%{"token" => token}, socket, _connect_info) do
    //       # max_age: 1209600 is equivalent to two weeks in seconds
    //       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
    //         {:ok, user_id} ->
    //           {:ok, assign(socket, :user, user_id)}
    //         {:error, reason} ->
    //           :error
    //       end
    //     end

    // -------------------------------------------------------------------
    // will match the url pattern '/items/:id'
    // then join on topic 'item:${item_id}'
    // let match = document.location.pathname.match(/\/items\/(d+)$/)

    // if (match) {
    //     let itemId = match[1]
    // }

    // let channel = socket.channel(`item:${itemId}`, {});

    // channel.on("new_bid", data => {
    //     console.log("new_bid message received", data)
    // });

    let channel = socket.channel('room:lobby', {});
    // create a new instance of Presence by passing in the channel
    let presence = new Presence(channel);
    let chatInput = document.querySelector('#chat-input');
    let messagesContainer = document.querySelector('#messages');

    function renderOnlineUsers(presence) {
        let response = '';

        presence.list((id, { metas: [first, ...rest] }) => {
            let count = rest.length + 1;
            response += `<br>${first.name} -- (count: ${count})</br>`;
        });

        document.getElementById('activeUsers').innerHTML = response;
    }

    socket.connect();

    presence.onSync(() => renderOnlineUsers(presence));

    // listen for keypress and then push "new_msg" to lobby
    chatInput.addEventListener('keypress', event => {
        if (event.keyCode === 13) {
            channel.push('new_msg', { body: chatInput.value });
            chatInput.value = '';
        }
    });

    // handle when "new_msg" gets pushed to the lobby
    channel.on('new_msg', payload => {
        let messageItem = document.createElement('li');
        messageItem.innerText = `[${payload.user}]  ${payload.body}`;
        messagesContainer.appendChild(messageItem);
    });

    channel
        .join()
        .receive('ok', resp => {
            console.log(
                'You have successfully joined the phoenix channel',
                resp
            );
        })
        .receive('error', resp => {
            console.log('Unable to join', resp);
        });
});

export default socket;
