# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :lynx,
  ecto_repos: [Lynx.Repo]

# Configures the endpoint
config :lynx, LynxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "p4xQb3O/tAFO8hDIT55lr/fvePfEj+KgRRZKcdm5p3nolEWyNjeHeqQj+jtDgKc7",
  render_errors: [view: LynxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Lynx.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure Ueberauth providers
config :ueberauth, Ueberauth,
  providers: [
    auth0: {Ueberauth.Strategy.Auth0, []}
  ]

# Configure Auth0
config :ueberauth, Ueberauth.Strategy.Auth0.OAuth,
  domain: System.get_env("AUTH0_DOMAIN"),
  client_id: System.get_env("AUTH0_CLIENT_ID"),
  client_secret: System.get_env("AUTH0_CLIENT_SECRET")

# Guardian configuration
config :lynx, Lynx.Accounts.Guardian,
  issuer: "LYNX",
  secret_key: System.get_env("GUARDIAN_SK")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
