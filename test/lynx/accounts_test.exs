defmodule Lynx.AccountsTest do
  @moduledoc """
  Testing the Lynx.Accounts context
  """
  use Lynx.DataCase

  alias Lynx.Accounts

  describe "users" do
    alias Lynx.Accounts.User

    @valid_attrs %{
      avatar: "some-image.png",
      display_name: "coolguy18",
      email: "anemail@aol.com",
      password: "abcd1234"
    }
    @update_attrs %{
      avatar: "new-image.png",
      display_name: "coolerguy19",
      email: "anewemail@aol.com",
      password: "passwordhash1212"
    }
    @invalid_attrs %{avatar: nil, display_name: nil, email: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.avatar == "some-image.png"
      assert user.display_name == "coolguy18"
      assert user.email == "anemail@aol.com"
      assert user.password_hash == "abcd1234"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert user.avatar == "new-image.png"
      assert user.display_name == "coolerguy19"
      assert user.email == "anewemail@aol.com"
      assert user.password_hash == "passwordhash1212"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
